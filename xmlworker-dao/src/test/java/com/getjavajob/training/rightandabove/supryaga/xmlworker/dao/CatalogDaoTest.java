package com.getjavajob.training.rightandabove.supryaga.xmlworker.dao;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Игорь on 27.07.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:dao-context.xml", "classpath:dao-context-overrides.xml"})
public class CatalogDaoTest {

    private static final int FULL_CATALOG_SIZE = 5;
    private static final int UPDATED_FULL_CATALOG_SIZE = 2;
    private static final int CATALOG_PAGE_SIZE = 5;
    private static final int CATALOG_PAGE_START_POSITION = 0;

    @Autowired
    private CatalogDao catalogDao;

    @Test
    public void getAll() {
        Catalog catalog = catalogDao.getAll();
        assertEquals(FULL_CATALOG_SIZE, catalog.getCdList().size());
        Cd cd = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        assertTrue(catalogDao.getAll().getCdList().contains(cd));
        cd = new Cd("Maggie May", "Rod Stewart", "UK", "Pickwick", 8.5, 1990);
        assertTrue(catalogDao.getAll().getCdList().contains(cd));
    }

    @Test
    public void getPage() {
        Catalog catalog = catalogDao.getPage(CATALOG_PAGE_START_POSITION, CATALOG_PAGE_SIZE);
        assertEquals(CATALOG_PAGE_SIZE, catalog.getCdList().size());
    }

    @Test
    public void updateCatalog() {
        Catalog beforeUpdate = prepareInitialCatalog();
        List<Cd> cds = new ArrayList<>();
        Cd cd1 = new Cd("One night only", "Bee Gees", "UK", "Polydor", 11.9, 1998);
        cds.add(cd1);
        Cd cd2 = new Cd("Test CD", "Test Artist", "UA", "RightAndAbove", 100, 2015);
        cds.add(cd2);
        Catalog catalog = new Catalog();
        catalog.setCdList(cds);
        catalogDao.updateCatalog(catalog);
        Catalog updated = catalogDao.getAll();
        assertEquals(UPDATED_FULL_CATALOG_SIZE, updated.getCdList().size());
        assertTrue(updated.getCdList().contains(cd1));
        assertTrue(updated.getCdList().contains(cd2));
        cd1 = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        assertFalse(updated.getCdList().contains(cd1));
        catalogDao.updateCatalog(beforeUpdate);
    }

    private Catalog prepareInitialCatalog() {
        Catalog beforeUpdate;
        List<Cd> cds = new ArrayList<>();
        Cd cd = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        cds.add(cd);
        cd = new Cd("Sylvias Mother", "Dr.Hook", "UK", "CBS", 8.1, 1973);
        cds.add(cd);
        cd = new Cd("Maggie May", "Rod Stewart", "UK", "Pickwick", 8.5, 1990);
        cds.add(cd);
        cd = new Cd("1999 Grammy Nominees", "Many", "USA", "Grammy", 10.2, 1999);
        cds.add(cd);
        cd = new Cd("When a man loves a woman", "Percy Sledge", "USA", "Grammy", 8.7, 1987);
        cds.add(cd);
        beforeUpdate = new Catalog();
        beforeUpdate.setCdList(cds);
        return beforeUpdate;
    }
}
