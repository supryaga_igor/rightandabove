package com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.xmlparsers;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Игорь on 24.07.2015.
 */
public class CatalogStaxParser {

    private File file;

    public CatalogStaxParser (File file) {
        this.file = file;
    }

    public Catalog getPieceOfCatalog(int start, int end) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader xmlStreamReader = null;
        try {
            xmlStreamReader = factory.createXMLStreamReader(new FileReader(file));
        } catch (XMLStreamException | FileNotFoundException e) {
            e.printStackTrace();
        }
        Catalog catalog = new Catalog();
        List<Cd> cds = new ArrayList<>();
        Cd cd = new Cd();
        String tagContent = "";
        int count = -1;
        try {
            while (xmlStreamReader.hasNext()) {
                int event = xmlStreamReader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT :
                        switch (xmlStreamReader.getLocalName()) {
                            case "CATALOG" :
                                catalog = new Catalog();
                                cds = new ArrayList<>();
                                break;
                            case "CD" :
                                count++;
                                if (count < start) {
                                    continue;
                                } else if (count > end) {
                                    catalog.setCdList(cds);
                                    return catalog;
                                }
                                cd = new Cd();
                                break;
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS :
                        tagContent = xmlStreamReader.getText();
                        break;
                    case XMLStreamConstants.END_ELEMENT :
                        switch (xmlStreamReader.getLocalName()) {
                            case "TITLE" :
                                cd.setTitle(tagContent);
                                break;
                            case "ARTIST" :
                                cd.setArtist(tagContent);
                                break;
                            case "COUNTRY" :
                                cd.setCountry(tagContent);
                                break;
                            case "COMPANY" :
                                cd.setCompany(tagContent);
                                break;
                            case "PRICE" :
                                cd.setPrice(Double.parseDouble(tagContent));
                                break;
                            case "YEAR" :
                                cd.setYear(Integer.parseInt(tagContent));
                                break;
                            case "CD" :
                                if (count >= start && count < end) {
                                    cds.add(cd);
                                }
                                break;
                            case "CATALOG" :
                                catalog.setCdList(cds);
                                break;
                        }
                        break;
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
        return catalog;
    }
}
