package com.getjavajob.training.rightandabove.supryaga.xmlworker.dao;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.exceptions.XmlWorkerDaoException;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.xmlparsers.CatalogStaxParser;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.stereotype.Component;

import java.io.*;

/**
 * Created by Игорь on 21.07.2015.
 */
@Component
public class CatalogDao {

    private static final Logger logger = org.apache.logging.log4j.LogManager.getLogger();

    @Value("${catalogFilePath}")
    private Resource catalogFilePath;
    @Autowired
    private XStreamMarshaller xStreamMarshaller;

    public Catalog getAll() {
        Catalog catalog;
        try (InputStream inputStream = catalogFilePath.getInputStream()) {
            catalog = (Catalog) xStreamMarshaller.unmarshalInputStream(inputStream);
            logger.debug("got all catalog from xml-catalog");
            return catalog;
        } catch (IOException e) {
            logger.error("failed to get all catalog from xml-catalog.", e);
            throw new XmlWorkerDaoException("Xml file not found", e);
        }
    }

    public Catalog getPage(int start, int end) {
        Catalog catalog;
        try {
            catalog = new CatalogStaxParser(catalogFilePath.getFile()).getPieceOfCatalog(start, end);
            logger.debug("got page from xml-catalog");
        } catch (IOException e) {
            logger.error(e.getMessage());
            throw new XmlWorkerDaoException("Can't find xml file", e);
        }
        return catalog;
    }

    public void deleteCd(Cd cd) {
        Catalog catalog = getAll();
        catalog.getCdList().remove(cd);
        updateCatalog(catalog);
    }

    public void updateCatalog(Catalog catalog) {
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream(catalogFilePath.getFile()))) {
            xStreamMarshaller.marshalOutputStream(catalog, os);
            logger.debug("catalog updated");
        } catch (IOException e) {
            logger.error("Catalog can't be update.", e);
            throw new XmlWorkerDaoException("Can't update catalog", e);
        }
    }
}
