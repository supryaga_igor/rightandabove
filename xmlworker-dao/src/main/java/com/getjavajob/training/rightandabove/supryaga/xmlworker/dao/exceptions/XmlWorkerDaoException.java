package com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.exceptions;

/**
 * Created by Игорь on 22.07.2015.
 */
public class XmlWorkerDaoException extends RuntimeException {
    public XmlWorkerDaoException(String s) {
        super(s);
    }

    public XmlWorkerDaoException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
