$(document).ready(function() {
    var table =  $('#example').DataTable( {
        "paging":   true,
        "ordering": false
    } );
    var row_index;
    var add_dialog;
    var modify_dialog;

    function deleteCd() {
        var title_name = 'title=';
        var artist_name = 'artist=';
        var country_name = 'country=';
        var company_name = 'company=';
        var price_name = 'price=';
        var year_name = 'year=';
        var redirect_page_name = '/deleteCd?';
        var delimeter = '&';
        row_index = row_index + 1;
        if (row_index > 10) {
            row_index = row_index % 10;
        }
        var title_value = title_name.concat(document.getElementById('example').rows[row_index].cells[0].innerHTML);
        var artist_value = artist_name.concat(document.getElementById('example').rows[row_index].cells[1].innerHTML);
        var country_value = country_name.concat(document.getElementById('example').rows[row_index].cells[2].innerHTML);
        var company_value = company_name.concat(document.getElementById('example').rows[row_index].cells[3].innerHTML);
        var price_value = price_name.concat(document.getElementById('example').rows[row_index].cells[4].innerHTML);
        var year_value = year_name.concat(document.getElementById('example').rows[row_index].cells[5].innerHTML);
        var redirect_url = redirect_page_name.concat(title_value, delimeter, artist_value, delimeter, country_value,
            delimeter, company_value, delimeter, price_value, delimeter, year_value);
        window.location = redirect_url;
    }

    // modal form for modifying cds
    modify_dialog = $("#modify-dialog-form").dialog({
        autoOpen: false,
        height: 800,
        width: 500,
        modal:true,
        buttons: {
            "Modify": function () {
                alert('add')
            },
            Cancel: function () {
                add_dialog.dialog("close");
            }
        },
        close: function () {
            form[0].reset();
            allFields.removeClass("ui-state-error");
        }
    });
    form = modify_dialog.find("form").on("submit", function (event) {
        event.preventDefault();
    });

    // modal form to add cds
    add_dialog = $("#add-dialog-form").dialog({
            autoOpen: false,
            height: 800,
            width: 500,
            modal:true,
            buttons: {
                "Create cd": function () {
                    alert('add')
                },
                Cancel: function () {
                    add_dialog.dialog("close");
                }
            },
            close: function () {
                form[0].reset();
                allFields.removeClass("ui-state-error");
            }
        });
        form = add_dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        });

    $('table').contextMenu('', {});

    $('#example tbody').on( 'click', 'tr', function () {
        row_index = table.row( this ).index();
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $(this).contextMenu('', {});
            document.getElementById("del_btn").disabled = true;
        } else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            $(this).contextMenu('myMenu1', {
                bindings: {
                    'delete': function(t) {
                        deleteCd();
                    },
                    'modify': function (t) {
                        modify_dialog.dialog("open");
                    }
                }
            });
            document.getElementById("del_btn").disabled = false;
        }
    });

    $('#del_btn').click( function () {
        deleteCd();
    } );

    $('#add_btn').click( function () {
        add_dialog.dialog("open");
    } );
} );

