<!doctype html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>XML viewer</title>
        <link rel="shortcut icon" href="/resources/pics/xml-ico.ico" />
        <link rel="stylesheet" href="/resources/css/style.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/datatables/1.10.7/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/jquery-ui/1.11.3/jquery-ui.css">
        <link rel="stylesheet" href="/resources/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="/resources/css/smoothy-jquery-ui.css">
        <link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <script src="${pageContext.request.contextPath}/webjars/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/webjars/datatables/1.10.7/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="/resources/js/dataTableScript.js" type="text/javascript"></script>
        <script src="/resources/js/jquery.contextmenu.js" type="text/javascript"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    </head>
    <jsp:include page="/WEB-INF/jsp/header.jsp"/>

    <div class="contextMenu non_visible" id="myMenu1">
        <ul>
            <%--<li id="modify"> <img src="/resources/pics/modify-ico.png"> Modify</li>--%>
            <li id="delete"> <img src="/resources/pics/del_btn_ico.png"> Delete</li>
        </ul>
    </div>

    <div id="headline" class="light_font">Catalog: </div>
    <div class="tbl_style">
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Title</th>
                <th>Artist</th>
                <th>Country</th>
                <th>Company</th>
                <th>Price</th>
                <th>Year</th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="cd" items="${catalog}">
                    <tr>
                        <td>${cd.title}</td>
                        <td>${cd.artist}</td>
                        <td>${cd.country}</td>
                        <td>${cd.company}</td>
                        <td>${cd.price}</td>
                        <td>${cd.year}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
<button id="del_btn" class="btn btn-danger operation_buttons light_font" disabled>Delete selected cd</button>
<%--<button id="add_btn" class="btn btn-primary operation_buttons light_font">Add new CD</button>--%>

<!-- non-visible forms for modal dialogs -->
    <div id="add-dialog-form" title="Add new CD" class="non_visible">
        <form>
            <fieldset>
                <label for="del-title">Title</label>
                <input type="text" name="title" id="del-title" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="del-artist">Artist</label>
                <input type="text" name="artist" id="del-artist" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="del-country">Country</label>
                <input type="text" name="country" id="del-country" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="del-company">Company</label>
                <input type="text" name="company" id="del-company" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="del-price">Price</label>
                <input type="text" name="price" id="del-price" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="del-year">Year</label>
                <input type="text" name="year" id="del-year" value="" class="text ui-widget-content ui-corner-all"/>
                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
        </form>
    </div>
    <div id="modify-dialog-form" title="Modify CD" class="non_visible">
        <form>
            <fieldset>
                <label for="add-title">Title</label>
                <input type="add-text" name="title" id="add-title" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="add-artist">Artist</label>
                <input type="text" name="artist" id="add-artist" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="add-country">Country</label>
                <input type="text" name="country" id="add-country" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="add-company">Company</label>
                <input type="text" name="company" id="add-company" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="add-price">Price</label>
                <input type="text" name="price" id="add-price" value="" class="text ui-widget-content ui-corner-all"/>
                <label for="add-year">Year</label>
                <input type="text" name="year" id="add-year" value="" class="text ui-widget-content ui-corner-all"/>
                <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
        </form>
    </div>
<!-- non-visible forms for modal dialogs -->
</body>
</html>