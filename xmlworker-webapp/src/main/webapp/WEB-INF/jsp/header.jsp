<!doctype html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Exo:300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<nav class="navbar navbar-inverse header_font">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" id="site_name" href="/viewXml">&lt;XmlWorker/&gt;</a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <c:choose>
                    <c:when test="${pageContext.request.servletPath eq '/WEB-INF/jsp/downloadXml.jsp'}">
                        <li><a href="/viewXml">View</a></li>
                        <li class = "active"><a href="/downloadXml">Download</a></li>
                        <li><a href="/upload">Upload</a></li>
                    </c:when>
                    <c:when test="${pageContext.request.servletPath eq '/WEB-INF/jsp/uploadXml.jsp'}">
                        <li><a href="/viewXml">View</a></li>
                        <li><a href="/downloadXml">Download</a></li>
                        <li class = "active"><a href="/upload">Upload</a></li>
                    </c:when>
                    <c:otherwise>
                        <li class = "active"><a href="/viewXml">View</a></li>
                        <li><a href="/downloadXml">Download</a></li>
                        <li><a href="/upload">Upload</a></li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </div>
</nav>
</body>
</html>
