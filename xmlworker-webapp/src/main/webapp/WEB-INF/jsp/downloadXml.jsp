<!doctype html>
<html>
<head>
    <title>Download XML</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/resources/pics/xml-ico.ico" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>
<div class="container text-center">
    <h2 class="light_font">Press the button to download XML:</h2>
    <form action="${pageContext.request.contextPath}/getXmlFile">
        <input type="submit" class="btn btn-info light_font btn_font_size" value="Download">
    </form>
</div>
</body>
</html>
