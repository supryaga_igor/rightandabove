<!doctype html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <title>Upload XML</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/resources/pics/xml-ico.ico" />
    <link href="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:200' rel='stylesheet' type='text/css'>
    <link href="${pageContext.request.contextPath}/webjars/bootstrap-fileinput/4.2.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
    <script src="${pageContext.request.contextPath}/webjars/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/webjars/jquery/1.11.3/jquery.min.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/webjars/bootstrap-fileinput/4.2.0/js/fileinput.min.js" type="text/javascript"></script>
</head>
<body>
<jsp:include page="/WEB-INF/jsp/header.jsp"/>
<div class="container text-center">
    <c:choose>
        <c:when test="${isSuccess == true}">
            <h2 class="bg-success light_font">${executionResult}</h2>
        </c:when>
        <c:otherwise>
            <h2 class="bg-danger light_font">${executionResult}</h2>
        </c:otherwise>
    </c:choose>
    <h2 class="light_font">Browse the file to upload into catalog :</h2>
    <form method="POST" action="uploadFile" class="form-group light_font" enctype="multipart/form-data">
        <input class="file" type="file" name="file">
    </form>
</div>
</body>
</html>
