package com.getjavajob.training.rightandabove.supryaga.xmlworker.webui.controllers;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.CatalogService;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.convertors.BytesToCatalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.webui.controllers.parametersConvertors.ParamsToCd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Игорь on 21.07.2015.
 */

@Controller
public class CatalogController {

    @Value("${catalogFilePath}")
    private Resource downloadPath;
    @Value("${pagesize}")
    private int pagesize;
    @Autowired
    private CatalogService catalogService;
    @Autowired
    private BytesToCatalog bytesToCatalog;

    @RequestMapping({"viewXml", "/"})
    public ModelAndView viewXml(@RequestParam(value = "startPosition", defaultValue = "0") int startPosition, Model model) {
        // link to dataTables guide page -> https://www.datatables.net/examples/basic_init/zero_configuration.html
        Catalog pageCatalog = catalogService.getAll();
        model.addAttribute("catalog", pageCatalog.getCdList());
        return new ModelAndView("viewXml");
    }

    @RequestMapping("upload")
    public ModelAndView upload() {
        return new ModelAndView("uploadXml");
    }

    @RequestMapping(value = "uploadFile", method = RequestMethod.POST)
    public String uploadXml(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
        String executionResult;
        boolean isSuccess = false;
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Catalog uploadedCatalog = bytesToCatalog.convert(bytes);
                catalogService.enrichCatalog(uploadedCatalog);
                executionResult = "File uploaded successfully";
                isSuccess = true;
            } catch (Exception e) {
                executionResult = "You failed to upload file, errorMessage = " + e.getMessage();
            }
        } else {
            executionResult = "You failed to upload because the file was empty.";
        }
        redirectAttributes.addFlashAttribute("executionResult", executionResult);
        redirectAttributes.addFlashAttribute("isSuccess", isSuccess);
        return "redirect:upload";
    }

    @RequestMapping("downloadXml")
    public ModelAndView downloadXml() {
        return new ModelAndView("downloadXml");
    }

    @RequestMapping("getXmlFile")
    @ResponseBody
    public FileSystemResource getXmlFile(HttpServletResponse response) throws IOException {
        response.setHeader("Content-Disposition", "attachment; filename=" + downloadPath.getFilename());
        return new FileSystemResource(downloadPath.getFile().getPath());
    }

    @RequestMapping("deleteCd")
    public String deleteCd(HttpServletRequest httpRequest,  RedirectAttributes redirectAttributes) {
        // TODO :After deletion of cd don't redirect to 1st page. For example from 4th.
        Cd cd = new ParamsToCd().convert(httpRequest);
        catalogService.deleteCd(cd);
        Catalog pageCatalog = catalogService.getAll();
        redirectAttributes.addFlashAttribute("catalog", pageCatalog.getCdList());
        return "redirect:viewXml";
    }
}
