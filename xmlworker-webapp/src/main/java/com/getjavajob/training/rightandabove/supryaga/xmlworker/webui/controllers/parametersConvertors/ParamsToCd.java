package com.getjavajob.training.rightandabove.supryaga.xmlworker.webui.controllers.parametersConvertors;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by user on 9/1/2015.
 */
public class ParamsToCd {
    public Cd convert(HttpServletRequest request) {
        String[] val = request.getParameterValues("artist");
        for(int i = 0; i < val.length; i++) {
            System.out.println(val[i]);
        }
        Cd cdToDel = new Cd(request.getParameter("title"), request.getParameter("artist"), request.getParameter("country"),
                request.getParameter("company"), Double.parseDouble(request.getParameter("price")),
                Integer.parseInt(request.getParameter("year")));
        return cdToDel;
    }
}
