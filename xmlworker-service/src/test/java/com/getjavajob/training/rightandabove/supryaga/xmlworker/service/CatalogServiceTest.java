package com.getjavajob.training.rightandabove.supryaga.xmlworker.service;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.CatalogDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;


/**
 * Created by Игорь on 27.07.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:service-context.xml" })
public class CatalogServiceTest {
    private static final int GET_PAGE_RESULT_SIZE_10 = 10;
    private static final int GET_PAGE_RESULT_SIZE_20 = 1;
    private static final int GET_PAGE_RESULT_SIZE_12 = 2;
    private static final int MERGED_CATALOG_SIZE = 11;

    @Mock
    private CatalogDao catalogDao;

    @InjectMocks
    @Autowired
    private CatalogService catalogService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getPage() {
        when(catalogDao.getPage(0, 10)).thenReturn(getPageResult10());
        when(catalogDao.getPage(10, 20)).thenReturn(getPageResult20());
        when(catalogDao.getPage(10, 12)).thenReturn(getPageResult12());

        assertEquals(GET_PAGE_RESULT_SIZE_10, catalogService.getPage(0, 10).getCdList().size());
        assertEquals(GET_PAGE_RESULT_SIZE_20, catalogService.getPage(10, 20).getCdList().size());
        assertEquals(GET_PAGE_RESULT_SIZE_12, catalogService.getPage(10, 12).getCdList().size());
    }

    @Test
    public void enrichCatalogs() {
        when(catalogDao.getAll()).thenReturn(getPageResult10());
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocationOnMock) throws Throwable {
                Catalog catalog = (Catalog) invocationOnMock.getArguments()[0];
                assertEquals(MERGED_CATALOG_SIZE, catalog.getCdList().size());
                Cd cd = new Cd("Test CD", "Bee Gees", "UK", "Polydor", 10.9, 1998);
                assertTrue(catalog.getCdList().contains(cd));
                cd = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
                assertTrue(catalog.getCdList().contains(cd));
                cd = new Cd("Sylvias Mother", "Dr.Hook", "UK", "CBS", 8.1, 1973);
                assertFalse(catalog.getCdList().contains(cd));
                cd = new Cd("When a man loves a woman", "Percy Sledge", "USA", "Grammy", 8.7, 1987);
                assertTrue(catalog.getCdList().contains(cd));
                return null;
            }
        }).when(catalogDao).updateCatalog(any(Catalog.class));

        catalogService.enrichCatalog(getPageResult12());
    }

    private Catalog getPageResult10() {
        Catalog result = new Catalog();
        List<Cd> cds = new ArrayList<>();
        Cd cd = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        cds.add(cd);
        cd = new Cd("Sylvias Mother", "Dr.Hook", "UK", "CBS", 8.1, 1973);
        cds.add(cd);
        cd = new Cd("Maggie May", "Rod Stewart", "UK", "Pickwick", 8.5, 1990);
        cds.add(cd);
        cd = new Cd("1999 Grammy Nominees", "Many", "USA", "Grammy", 10.2, 1999);
        cds.add(cd);
        cd = new Cd("When a man loves a woman", "Percy Sledge", "USA", "Grammy", 8.7, 1987);
        cds.add(cd);
        cd = new Cd("Eros", "Eros Ramazzotti", "EU", "BMG", 9.9, 1997);
        cds.add(cd);
        cd = new Cd("Romanza", "Andrea Bocelli", "EU", "Polydor", 10.8, 1996);
        cds.add(cd);
        cd = new Cd("Still got the blues", "Gary Moore", "UK", "Virgin records", 10.2, 1990);
        cds.add(cd);
        cd = new Cd("Black angel", "Savage Rose", "EU", "Mega", 10.9, 1995);
        cds.add(cd);
        cd = new Cd("Empire Burlesque", "Bob Dylan", "USA", "Columbia", 777.9, 1985);
        cds.add(cd);
        result.setCdList(cds);
        return result;
    }

    private Catalog getPageResult12() {
        Catalog result = new Catalog();
        List<Cd> cds = new ArrayList<>();
        Cd cd = new Cd("Test CD", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        cds.add(cd);
        cd = new Cd("Sylvias Mother", "Dr.Hook", "UK", "CBS", 18.1, 1973);
        cds.add(cd);
        result.setCdList(cds);
        return result;
    }

    private Catalog getPageResult20() {
        Catalog result = new Catalog();
        List<Cd> cds = new ArrayList<>();
        Cd cd = new Cd("One night only", "Bee Gees", "UK", "Polydor", 10.9, 1998);
        cds.add(cd);
        result.setCdList(cds);
        return result;
    }
}
