package com.getjavajob.training.rightandabove.supryaga.xmlworker.service.convertors;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions.XmlWorkerServiceException;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions.XmlWorkerServiceValidationException;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.validators.XmlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by Игорь on 25.07.2015.
 */
@Component
public class BytesToCatalog {

    @Autowired
    private XmlValidator xmlValidator;
    @Autowired
    private XStreamMarshaller xStreamMarshaller;

    public Catalog convert(byte[] byteArray) throws XmlWorkerServiceValidationException {
        Catalog catalog = null;
        String bytesToString = new String(byteArray);
        try {
            xmlValidator.validate(bytesToString);
        } catch (XmlWorkerServiceValidationException e) {
            throw new XmlWorkerServiceValidationException("Can't convert bytes to catalog", e);
        }
        try {
            catalog = (Catalog) xStreamMarshaller.unmarshalReader(new StringReader(bytesToString));
        } catch (IOException e) {
            throw new XmlWorkerServiceException("Can't convert bytes", e);
        }
        return catalog;
    }
}
