package com.getjavajob.training.rightandabove.supryaga.xmlworker.service;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Catalog;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.common.Cd;
import com.getjavajob.training.rightandabove.supryaga.xmlworker.dao.CatalogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by Игорь on 21.07.2015.
 */
@Component
public class CatalogService {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private CatalogDao catalogDao;

    public Catalog getPage(int startPosition, int endPosition) {
        logger.debug("get page with start position = " + startPosition + " and end position = " + endPosition);
        return catalogDao.getPage(startPosition, endPosition);
    }

    public Catalog getAll() {
        logger.debug("get full catalog");
        return catalogDao.getAll();
    }

    public void deleteCd(Cd cd) {
        catalogDao.deleteCd(cd);
    }

    public void enrichCatalog(Catalog uploadedCatalog) {
        Catalog internalCatalog = catalogDao.getAll();
        Catalog mergedCatalog = mergeCatalogs(internalCatalog, uploadedCatalog);
        catalogDao.updateCatalog(mergedCatalog);
        logger.debug("catalog updated successfully");
    }

    private Catalog mergeCatalogs(Catalog internalCatalog, Catalog externalCatalog) {
        Map<String, Cd> internalCds = listToMap(internalCatalog.getCdList());
        Map<String, Cd> uploadedCds = listToMap(externalCatalog.getCdList());
        internalCds.putAll(uploadedCds);
        List<Cd> list = new ArrayList<>(internalCds.values());
        Catalog catalog = new Catalog();
        catalog.setCdList(list);
        logger.debug("catalogs merged");
        return catalog;
    }

    private Map listToMap(List<Cd> list) {
        Map<String, Cd> map = new HashMap<>();
        for (Cd cd : list) {
            map.put(cd.getTitle(), cd);
        }
        return map;
    }
}
