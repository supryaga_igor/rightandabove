package com.getjavajob.training.rightandabove.supryaga.xmlworker.service.validators;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions.XmlWorkerServiceValidationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import org.springframework.core.io.Resource;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.StringReader;

/**
 * Created by Игорь on 21.07.2015.
 */

@Component
public class XmlValidator implements Validator {

    @Value("${catalogXsdFilePath}")
    private Resource catalogXsdFilePath;

    @Override
    public void validate(String xml) throws XmlWorkerServiceValidationException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = factory.newSchema(catalogXsdFilePath.getFile());
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
        } catch (SAXException | IOException e) {
           throw new XmlWorkerServiceValidationException("Can't validate file", e);
        }
    }
}
