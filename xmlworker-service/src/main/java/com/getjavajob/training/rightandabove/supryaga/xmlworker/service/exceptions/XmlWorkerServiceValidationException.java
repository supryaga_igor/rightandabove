package com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions;

/**
 * Created by Игорь on 21.07.2015.
 */
public class XmlWorkerServiceValidationException extends Exception {
    public XmlWorkerServiceValidationException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
