package com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions;

/**
 * Created by Игорь on 25.07.2015.
 */
public class XmlWorkerServiceException extends RuntimeException {
    public XmlWorkerServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
