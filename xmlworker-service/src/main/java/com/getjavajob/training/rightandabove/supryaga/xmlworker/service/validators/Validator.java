package com.getjavajob.training.rightandabove.supryaga.xmlworker.service.validators;

import com.getjavajob.training.rightandabove.supryaga.xmlworker.service.exceptions.XmlWorkerServiceValidationException;

/**
 * Created by Игорь on 21.07.2015.
 */
public interface Validator {
    void validate(String xml) throws XmlWorkerServiceValidationException;
}
