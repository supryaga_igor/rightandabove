package com.getjavajob.training.rightandabove.supryaga.xmlworker.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Игорь on 22.07.2015.
 */
@XStreamAlias("CATALOG")
public class Catalog {
    @XStreamImplicit(itemFieldName = "CD")
    private List<Cd> cdList = new ArrayList<>();

    public List<Cd> getCdList() {
        return cdList;
    }

    public void setCdList(List<Cd> cdList) {
        this.cdList = cdList;
    }
}