package com.getjavajob.training.rightandabove.supryaga.xmlworker.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Игорь on 22.07.2015.
 */

@XStreamAlias("CD")
public class Cd {
    @XStreamAlias("TITLE")
    private String title;
    @XStreamAlias("ARTIST")
    private String artist;
    @XStreamAlias("COUNTRY")
    private String country;
    @XStreamAlias("COMPANY")
    private String company;
    @XStreamAlias("PRICE")
    private double price;
    @XStreamAlias("YEAR")
    private int year;

    public Cd() {
    }

    public Cd(String title, String artist, String country, String company, double price, int year) {
        this.title = title;
        this.artist = artist;
        this.country = country;
        this.company = company;
        this.price = price;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cd)) return false;

        Cd cd = (Cd) o;

        if (Double.compare(cd.price, price) != 0) return false;
        if (year != cd.year) return false;
        if (!artist.equals(cd.artist)) return false;
        if (!company.equals(cd.company)) return false;
        if (!country.equals(cd.country)) return false;
        if (!title.equals(cd.title)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = title.hashCode();
        result = 31 * result + artist.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + company.hashCode();
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) year;
        return result;
    }

    @Override
    public String toString() {
        return "Cd{" +
                "title='" + title + '\'' +
                ", artist='" + artist + '\'' +
                ", country='" + country + '\'' +
                ", company='" + company + '\'' +
                ", price=" + price +
                ", year=" + year +
                '}';
    }
}
